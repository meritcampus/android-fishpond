package com.example.androidfishpond;

import java.io.InputStream;
import java.net.URLEncoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@SuppressLint("NewApi")
public class NewFishPond extends Activity implements OnClickListener {
	EditText name, message;
	FishPond fishPond;
	ConnectivityManager connectivityManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_fish_pond);
		name = (EditText) findViewById(R.id.name_id);
		message = (EditText) findViewById(R.id.message_id);
		name.requestFocus();
		Button button = (Button) findViewById(R.id.button1);
		connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		button.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		fishPond = new FishPond();
	}
	@Override
	public void onClick(View view) {
		
		if (!fishPond.isNetworkAvailable(connectivityManager)) {
			Toast.makeText(getApplicationContext(),"Check Your Internet Connection..", Toast.LENGTH_LONG).show();
			startActivity(new Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS));
		} else {
			if (view.getId() == R.id.button1) {
				if (!name.getText().toString().isEmpty()
						&& !message.getText().toString().isEmpty()) {
					StrictMode.enableDefaults();
					DefaultHttpClient httpClient = new DefaultHttpClient();
					try {

						HttpGet httpGet = new HttpGet("http://java.meritcampus.com//fish_pond_apps/create_pond.json?person_name="+ URLEncoder.encode(name.getText().toString(), "UTF-8")+ "&message="+ URLEncoder.encode(message.getText().toString(), "UTF-8"));
						Log.d("Sent URL","http://http://java.meritcampus.com//fish_pond_apps/create_pond.json?person_name="+ URLEncoder.encode(name.getText().toString(), "UTF-8")+ "&message="+ message.getText().toString());
						HttpResponse httpResponse = httpClient.execute(httpGet);
						HttpEntity httpEntity = httpResponse.getEntity();
						InputStream isr = httpEntity.getContent();

						setResult(RESULT_OK);
						this.finish();

					} catch (Exception e) {
						e.printStackTrace();
						Toast.makeText(getApplicationContext(),"Sorry you are unable to reach the server!!! =)",Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(getApplicationContext(),"Fields should not be empty ", Toast.LENGTH_LONG).show();
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
