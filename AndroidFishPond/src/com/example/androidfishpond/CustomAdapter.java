package com.example.androidfishpond;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter {
	List<FishPondDetails> fishPondList = new ArrayList<FishPondDetails>();
	Context context;
	int resourceId;

	CustomAdapter(Context context, int id, List<FishPondDetails> list) {
		this.context = context;
		this.resourceId = id;
		this.fishPondList = list;

	}

	@Override
	public int getCount() {

		return fishPondList.size();
	}

	@Override
	public FishPondDetails getItem(int position) {

		return fishPondList.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.list_row, null);
		// get id from xml file
		TextView name = (TextView)view.findViewById(R.id.name);
		TextView message = (TextView)view.findViewById(R.id.message);
		TextView numOfLikes = (TextView)view.findViewById(R.id.numLike);
		ImageButton likeImage = (ImageButton)view.findViewById(R.id.imageView);
				name.setText(fishPondList.get(position).name);
		message.setText(fishPondList.get(position).message);
		numOfLikes.setText(fishPondList.get(position).likes+"");
		
		// onclick for like item
		likeImage.setOnClickListener(createOnClickListener(position, parent));
		return view;
	}
	public OnClickListener createOnClickListener(final int position, final ViewGroup parent) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((ListView) parent).performItemClick(v, position, 0);
            }
        };
    }
	public void setFishPondList(List<FishPondDetails> fishPondDetails){
		this.fishPondList = fishPondDetails;
		notifyDataSetChanged();
	}

	

}
