package com.example.androidfishpond;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;





public class FishPond extends Activity implements OnClickListener,
OnItemClickListener {
List<FishPondDetails> fishPondsList = new ArrayList<FishPondDetails>();

ConnectivityManager connectivityManager;
ListView listView;
CustomAdapter adapter;

@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_fish_pond);

Button createButton = (Button) findViewById(R.id.create_button_id);
listView = (ListView) findViewById(R.id.listView);
createButton.setOnClickListener(this);

connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
if (!isNetworkAvailable(connectivityManager)) {
	Toast.makeText(getApplicationContext(),
			"Check Your Internet Connection..", Toast.LENGTH_LONG)
			.show();
	startActivity(new Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS));
} else {
	getAllFishPonds();

	fishPondsList = getSortedList(fishPondsList);
	adapter = new CustomAdapter(getApplicationContext(),
			R.layout.list_row, fishPondsList);
	listView.setAdapter(adapter);
}
listView.setOnItemClickListener(this);

}
@Override
public void onClick(View v) {
switch (v.getId()) {
case R.id.create_button_id:
	Intent intent = new Intent(this, NewFishPond.class);
	startActivityForResult(intent, 1);
	break;

}
}


@SuppressLint("NewApi")
public void getAllFishPonds() {
StrictMode.enableDefaults();
String content = getFishPondsFromSite();

try {
	JSONArray jsonArray = new JSONArray(content);
	if (jsonArray.length() > 0) {
		fishPondsList.clear();
		// likes.clear();
	}

	for (int i = 0; i < jsonArray.length(); i++) {
		JSONObject jsonObject = jsonArray.getJSONObject(i);
		FishPondDetails fishPond = new FishPondDetails();
		fishPond.name = jsonObject.getString("name_of_the_person");
		fishPond.message = jsonObject.getString("message");
		fishPond.likes = jsonObject.getInt("likes");
		fishPond.id = jsonObject.getInt("id");
		fishPondsList.add(fishPond);
	}
} catch (JSONException e) {
	
	e.printStackTrace();
}
}

public String getFishPondsFromSite() {
String result = "";
InputStream isr = null;
DefaultHttpClient httpClient = new DefaultHttpClient();
try {

	HttpGet httpGet = new HttpGet(
			"http://java.meritcampus.com/fish_pond_apps/all_ponds.json");
	HttpResponse httpResponse = httpClient.execute(httpGet);
	HttpEntity httpEntity = httpResponse.getEntity();
	isr = httpEntity.getContent();
} catch (Exception e) {
	e.printStackTrace();
	Toast.makeText(getApplicationContext(),
			"Sorry you are unable to reach the server!!! =)",
			Toast.LENGTH_LONG).show();
}
try {

	BufferedReader bufferedReader = new BufferedReader(
			new InputStreamReader(isr, "iso-8859-1"), 8);
	StringBuilder sb = new StringBuilder();
	String line = null;
	while ((line = bufferedReader.readLine()) != null) {
		sb.append(line + "\n");
	}
	isr.close();
	result = sb.toString();

} catch (Exception e) {
	e.printStackTrace();
}
return result;
}

public List<FishPondDetails> getSortedList(
	List<FishPondDetails> fishPondsData) {
Collections.sort(fishPondsData, new Comparator<FishPondDetails>() {
	@SuppressLint("NewApi")
	@Override
	public int compare(FishPondDetails f1, FishPondDetails f2) {
		if (f1.likes > f2.likes)
			return -1;
		if (f1.likes < f2.likes)
			return 1;
		return 0;
	}
});
return fishPondsData;
}

public boolean isNetworkAvailable(ConnectivityManager connectivityManager) {
// ConnectivityManager connectivityManager = (ConnectivityManager)
// getSystemService(Context.CONNECTIVITY_SERVICE);
NetworkInfo activeNetworkInfo = connectivityManager
		.getActiveNetworkInfo();
return activeNetworkInfo != null
		&& activeNetworkInfo.isConnectedOrConnecting();

}



@Override
protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
if (1 == requestCode && resultCode == RESULT_OK) {
	if (!isNetworkAvailable(connectivityManager)) {
		Toast.makeText(getApplicationContext(),
				"Check Your Internet Connection..", Toast.LENGTH_LONG)
				.show();
		startActivity(new Intent(
				Settings.ACTION_NETWORK_OPERATOR_SETTINGS));
	} else {
		getAllFishPonds();
		getSortedList(fishPondsList);
		listView.setAdapter(adapter);
	}
}
}

@Override
public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
TextView likeText = (TextView) v.findViewById(R.id.numLike);
FishPondDetails details = fishPondsList.get(position);

DefaultHttpClient httpClient = new DefaultHttpClient();

HttpGet httpGet = new HttpGet(
		"http://java.meritcampus.com//fish_pond_apps/like_pond?id="
				+ details.id + "");
HttpResponse httpResponse;
try {
	httpResponse = httpClient.execute(httpGet);
	HttpEntity httpEntity = httpResponse.getEntity();
	//likeText.setText(details.likes++ + "");
	details.likes++;
	
	getSortedList(fishPondsList);
	adapter.setFishPondList(fishPondsList);
	listView.setAdapter(adapter);
	
} catch (Exception e) {
	e.printStackTrace();
}

}
@Override
public boolean onCreateOptionsMenu(Menu menu) {
getMenuInflater().inflate(R.menu.main, menu);
return true;
}

@Override
public boolean onOptionsItemSelected(MenuItem item) {
int id = item.getItemId();
if (id == R.id.action_settings) {
	return true;
}
return super.onOptionsItemSelected(item);
}


	
}